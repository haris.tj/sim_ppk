<?php

class RealisasiModel extends CI_Model
{
	
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}	
	//list
	public function getAll(){
		$this->db->select('*');
		$this->db->from('tb_realisasi');
		$query = $this->db->get();
		return $query->result();
	}	
	// detail peruser
	public function detail($id_realisasi){
		$query = $this->db->get_where('tb_realisasi',array('id_realisasi'  => $id_realisasi));
		return $query->row();
	}// Edit 
	public function edit ($data,$id) {
		$this->db->where('id_realisasi',$id);
		$this->db->update('tb_realisasi',$data);
	}
	
	// Delete
	public function delete ($data){
		$this->db->where('id_realisasi',$data['id_realisasi']);
		$this->db->delete('tb_realisasi',$data);
	}
		public function insert ($data) {
			$this->db->insert('tb_realisasi',$data);
		}

	} ?>