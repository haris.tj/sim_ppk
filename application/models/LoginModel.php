<?php
class LoginModel extends CI_Model{
	function __construct() {
		parent::__construct();
	}
	function validate($username,$password){
		$this->db->where('username',$username);
		$this->db->where('password',$password);
		$result = $this->db->get('tb_user',1);
		return $result;
	}

	function getDetail(){
		$this->load->library('Simple_login');
		$this->load->library('session');
		$id = $this->session->userdata('id');
		$query = $this->db->get_where('tb_user',array('id'  => $id));
		return $query->row();
	}
	
		
}
?>