<?php

class DashModel extends CI_Model
{
	
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function jumlahuser(){   
		$query = $this->db->get('tb_user');
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		else
		{
			return 0;
		}
	}
	public function jumlahgaleri(){   
		$query = $this->db->get('tb_galeri');
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		else
		{
			return 0;
		}
	}
	public function jumlahprogram(){   
		$query = $this->db->get('tb_program');
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		else
		{
			return 0;
		}
	}
} ?>