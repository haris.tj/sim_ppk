<?php

class GaleriModel extends CI_Model
{
	
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	//list
	public function getAll(){
		$this->db->select('*');
		$this->db->from('tb_galeri');
		$query = $this->db->get();
		return $query->result();
	}	
	// detail peruser
	public function detail($id){
		$query = $this->db->get_where('tb_galeri',array('id_gambar'  => $id));
		return $query->row();
	}	
	// Delete
	public function delete ($data){
		$this->db->where('id_gambar',$data['id_gambar']);
		$this->db->delete('tb_galeri',$data);
	}
	public function insert ($data) {
		$this->db->insert('tb_galeri',$data);
	}
	public function edit ($data) {
		$this->db->where('id_gambar',$data['id_gambar']);
		$this->db->update('tb_galeri',$data);
	}
	
} ?>