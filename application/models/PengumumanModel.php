<?php

class PengumumanModel extends CI_Model
{
	
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	//list
	public function getAll(){
		$this->db->select('*');
		$this->db->from('tb_pengumuman');
		$query = $this->db->get();
		return $query->result();
	}	
	// detail peruser
	public function detail($id_pengumuman){
		$query = $this->db->get_where('tb_pengumuman',array('id_pengumuman'  => $id_pengumuman));
		return $query->row();
	}// Edit 
	public function edit ($data,$id) {
		$this->db->where('id_pengumuman',$id);
		$this->db->update('tb_pengumuman',$data);
	}
	
	// Delete
	public function delete ($data){
		$this->db->where('id_pengumuman',$data['id_pengumuman']);
		$this->db->delete('tb_pengumuman',$data);
	}
	public function insert ($data) {
		$this->db->insert('tb_pengumuman',$data);
	}
	
} ?>