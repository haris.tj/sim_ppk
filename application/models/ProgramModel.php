<?php

class ProgramModel extends CI_Model
{
	
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	//list
	public function getAll(){
		$this->db->select('*');
		$this->db->from('tb_program');
		$query = $this->db->get();
		return $query->result();
	}	
	// detail peruser
	public function detail($id){
		$query = $this->db->get_where('tb_program',array('id_program'  => $id));
		return $query->row();
	}	
	// Delete
	public function delete ($data){
		$this->db->where('id_program',$data['id_program']);
		$this->db->delete('tb_program',$data);
	}
	public function insert ($data) {
		$this->db->insert('tb_program',$data);
	}
	public function edit ($data,$id) {
		$this->db->where('id_program',$id);
		$this->db->update('tb_program',$data);
	}
	
} ?>