<?php

class UserModel extends CI_Model
{
	
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	//list
	public function getAll(){
		$this->db->select('*');
		$this->db->from('tb_user');
		$query = $this->db->get();
		return $query->result();
	}	
	// detail peruser
	public function detail($id_user){
		$query = $this->db->get_where('tb_user',array('id'  => $id_user));
		return $query->row();
	}// Edit 
	public function edit ($data,$id_user) {
		$this->db->where('id',$id_user);
		$this->db->update('tb_user',$data);
	}
	
	// Delete
	public function delete ($data){
		$this->db->where('id',$data['id']);
		$this->db->delete('tb_user',$data);
	}
	public function insert ($data) {
		$this->db->insert('tb_user',$data);
	}
	
} ?>