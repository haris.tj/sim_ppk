<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('dashmodel');
	}

	public function index()
	{
		$user = $this->dashmodel->jumlahuser();
		$galeri = $this->dashmodel->jumlahgaleri();
		$program = $this->dashmodel->jumlahprogram();
		$data = array(
			'user' => $user,
			'galeri' => $galeri,
			'program' => $program,
			'isi' => 'user/dashboard');
		$this->load->view('layout/wrapper',$data);

	}
}

?>
