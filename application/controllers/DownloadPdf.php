<?php 
	/**
	 * 
	 */
	class DownloadPdf extends CI_Controller
	{
		
		function __construct() {
			parent::__construct();
			$this->load->library('pdf');
		}

		function index(){
			$pdf = new FPDF('l','mm','A5');
        // membuat halaman baru
			$pdf->AddPage();
        // setting jenis font yang akan digunakan

			$pdf->SetFont('Arial','B',12);
			$pdf->Cell(190,7,'Perencanaan Program Kerja Jemaat Gereja Kalvari Jemaat Getsemani Sorong, Papua Barat',0,1,'C');
        // Memberikan space kebawah agar tidak terlalu rapat
			$pdf->Cell(10,7,'',0,1);
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(8,6,'No',1,0);
			$pdf->Cell(30,6,'Nama',1,0);
			$pdf->Cell(20,6,'Periode',1,0);
			$pdf->Cell(27,6,'Waktu',1,0);
			$pdf->Cell(25,6,'Pelaksanaan',1,1);
			$pdf->SetFont('Arial','',10);
			$program = $this->db->get('tb_program')->result();
			$no = 1;
			foreach ($program as $row){
				$pdf->cell(8,6,$no++,1,0);
				$pdf->Cell(30,6,$row->nama_program,1,0);
				$pdf->Cell(20,6,$row->periode,1,0);
				$pdf->Cell(27,6,$row->waktu,1,0);
				$pdf->Cell(25,6,$row->pelaksanaan,1,1); 
			}
			$pdf->Output();
		}
	}
	?>