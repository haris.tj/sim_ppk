<?php 
/**
 * 
 */
class Login extends CI_Controller
{
	function __construct(){
		parent::__construct();
		$this->load->model('loginmodel');
	}
	public function index() {
		
		// Validasi
		$valid = $this->form_validation;
		
		$valid->set_rules('username','Username','required',
			array(	'required'	=> 'Username harus diisi'));
		$valid->set_rules('password','Password','required',
			array(	'required'	=> 'Password harus diisi'));
		
		$username	= $this->input->post('username');
		$password	= $this->input->post('password');
		
		if($valid->run()) {  

			$this->simple_login->login($username,$password,
				base_url('dashboard'), base_url('login'));
		}
		// End validasi
		$this->load->view('loginview');
	}
	
	
	// Logout
	public function logout() {
		$this->simple_login->logout();
	}
}
?>