<?php  
class Realisasi extends CI_Controller
{
	function __construct(){
		parent::__construct();
		$this->load->model('realisasimodel');
		$this->load->model('programmodel');
	}
	public function index()
	{
		
		$realis = $this->realisasimodel->getAll();
		$data = array(
			'realis' => $realis,
			'isi' => 'realisasi/list'
		);
		$this->load->view('layout/wrapper',$data);
	}
	public function delete($id_realisasi){
		$data = array('id_realisasi'	=> $id_realisasi);
		$this->realisasimodel->delete($data);
		$this->session->set_flashdata('sukses','Data telah dihapus');
		redirect(base_url('pages/realisasi'));	
	}
	public function update($id){
		$x = $this->realisasimodel->detail($id);
		$valid = $this->form_validation;
		
		$valid->set_rules('nama_realisasi','nama_realisasi','required',
			array( 'required' => 'Nama realisasi harus diisi'));

		$valid->set_rules('file','file','required',
			array( 'required' => 'File harus diisi'));
		
		if($valid->run()===FALSE) {
		// End validasi
			$data = array(
				'data' => $x,
				'isi'  => 'realisasi/update');
			$this->load->view('layout/wrapper',$data);
		// masuk database
		}else{
			$config['upload_path'] = './assets/upload/files/';
			$config['allowed_types'] = 'pdf|doc|docx';
			$config['file_name']	= rand();
        // load library upload
			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('file')) {
				$error = $this->upload->display_errors();
				print_r($error);
			} else {
				//$result = $this->upload->data();
				$upload_data = array('uploads' =>$this->upload->data());
				$i = $this->input;
				$data = array( 
					'nama_realisasi'			=>  $i->post('nama_realisasi'),
					'tempat'					=>  $i->post('tempat'),
					'periode'					=>  $i->post('periode'),
					'waktu'						=>  $i->post('waktu'),
					'pelaksanaan'				=>  $i->post('pelaksanaan'),
					'anggaran'					=>  $i->post('anggaran'),
					'laporan'					=> 	$upload_data['uploads']['file_name'],
					'deskripsi'					=>  $i->post('deskripsi'));
				$this->realisasimodel->edit($data,$id);
				$this->session->set_flashdata('sukses','Data telah Dirubah');
				redirect(base_url('dashboard'));
			}
			
		}
	}

	public function realisasi($id){
		$query = $this->db->query("insert into realisasi(id_realisasi,id_program,nama_realisasi,deskripsi,periode,waktu,pelaksanaan,tempat,anggaran,laporan) select * from tb_realisasi where id_realisasi=".$id);
		if ($query) {
			$this->session->set_flashdata('sukses','Data Berhasil Direalisasikan');
			redirect(base_url('pages/realisasi'));
		}
	}
	
}
?>