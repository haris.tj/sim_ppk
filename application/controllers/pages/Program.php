<?php /**
 * 
 */
class Program extends CI_Controller
{
	
	public function __construct() {
		parent::__construct();
		$this->load->model('programmodel');
		$this->load->model('RealisasiModel');
		$this->load->library('Simple_login');
		$this->load->library('session');
	}

	public function index()
	{
		$user = $this->programmodel->getAll();
		$data = array(
			'user' => $user,
			'isi' => 'program/list'
		);
		$this->load->view('layout/wrapper',$data);
	}
	public function delete($id_program) {
		$this->simple_login->cek_login();
		$data = array('id_program'	=> $id_program);
		$this->programmodel->delete($data);
		$this->session->set_flashdata('sukses','Data telah didelete');
		redirect(base_url('pages/program'));	
		
	}
	public function insert(){
		$valid = $this->form_validation;
		
		$valid->set_rules('nama_program','nama_program','required',
			array( 'required' => 'Nama Program harus diisi'));
		
		if($valid->run()===FALSE) {
		// End validasi

			$data = array('isi'  => 'program/insert');
			$this->load->view('layout/wrapper',$data);
		// masuk database
		}else{
			$i = $this->input;
			$data = array( 
				'nama_program' => $i->post('nama_program'),
				'deskripsi' => $i->post('deskripsi'),
				'periode' => $i->post('periode'),
				'waktu' => $i->post('waktu'),
				'pelaksanaan' => $i->post('pelaksanaan'),
				'tempat' => $i->post('tempat'),
				'anggaran' => $i->post('anggaran'));
			$this->programmodel->insert($data);
			$this->session->set_flashdata('sukses','user telah ditambah');
			redirect(base_url('pages/program'));
		}
	}
	public function update($id){

		$data = $this->programmodel->detail($id);
		
		// Validasi
		$valid = $this->form_validation;
		$valid->set_rules('nama_program','nama_program','required',
			array( 'required' => 'Nama Program harus diisi'));

		if($valid->run()===FALSE) {
		// End validasi

			$data = array(
				'prog'	=> $data,
				'isi' 	=> 'program/update');
			$this->load->view('layout/wrapper',$data);
		// masuk database
		}else{
			$username = $this->session->userdata('username');
			$i = $this->input;
			$data = array( 
				'nama_program' => $i->post('nama_program'),
				'deskripsi' => $i->post('deskripsi'),
				'periode' => $i->post('periode'),
				'waktu' => $i->post('waktu'),
				'pelaksanaan' => $i->post('pelaksanaan'),
				'tempat' => $i->post('tempat'),
				'anggaran' => $i->post('anggaran'));
			$this->programmodel->edit($data,$id);
			$this->session->set_flashdata('sukses','Data Berhasil Dirubah');
			redirect(base_url('pages/program'));
		}
	}

	public function realis($id){
		$data = $this->programmodel->detail($id);
		$valid = $this->form_validation;
		
		$valid->set_rules('nama_realisasi','nama_realisasi','required',
			array( 'required' => 'Nama realisasi harus diisi'));
		
		if($valid->run()===FALSE) {
		// End validasi
			$data = array(
				'data' => $data,
				'isi'  => 'realisasi/insert');
			$this->load->view('layout/wrapper',$data);
		// masuk database
		}else{
			$config['upload_path'] = './assets/upload/files/';
			$config['allowed_types'] = 'pdf|doc|docx';
			$config['file_name']	= rand();
        // load library upload
			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('file')) {
				$error = $this->upload->display_errors();
				print_r($error);
			} else {
				//$result = $this->upload->data();
				$upload_data = array('uploads' =>$this->upload->data());
				$i = $this->input;
				$data = array( 
					'id_program'				=> 	$data->id_program,
					'nama_realisasi'			=>  $i->post('nama_realisasi'),
					'tempat'					=>  $i->post('tempat'),
					'periode'					=>  $i->post('periode'),
					'waktu'						=>  $i->post('waktu'),
					'pelaksanaan'				=>  $i->post('pelaksanaan'),
					'anggaran'					=>  $i->post('anggaran'),
					'laporan'					=> 	$upload_data['uploads']['file_name'],
					'deskripsi'					=>  $i->post('deskripsi'));
				$this->RealisasiModel->insert($data);
				$this->session->set_flashdata('sukses','Data telah ditambah');
				redirect(base_url('dashboard'));
			}
			
		}
	}


} ?>