<?php  

/**
 * 
 */
class Pengumuman extends CI_Controller
{
	function __construct(){
		parent::__construct();
		$this->load->model('Pengumumanmodel');
	}

	public function index()
	{
		$data = $this->Pengumumanmodel->getAll();
		$data = array(
			'pengumuman' => $data,
			'isi' => 'Pengumuman/list'
		);
		$this->load->view('layout/wrapper',$data);
	}

	public function insert(){
		$valid = $this->form_validation;

		$valid->set_rules('judul','judul','required|is_unique[tb_pengumuman.judul]',
			array( 	'required' 	=> 'judul harus diisi',
				'is_unique'	=> 'judul: <strong>'.$this->input->post('judul').
				'</strong> sudah digunakan. Buat judul baru!'));
		
		if($valid->run()===FALSE) {
		// End validasi

			$data = array('isi'  => 'pengumuman/insert');
			$this->load->view('layout/wrapper',$data);
			$role = $this->session->userdata('roleid');
			if ($role != 1) {
				redirect(base_url('login/logout'));
			}
		// masuk database
		}else{
			$i = $this->input;
			$data = array( 
				'judul' => $i->post('judul'),
				'konten' => $i->post('pengumuman'),
				'tanggal_post' => date('Y-m-d H:i:s'));
			$this->Pengumumanmodel->insert($data);
			$this->session->set_flashdata('sukses','Pengumuman telah ditambah');
			redirect(base_url('pages/Pengumuman'));
		}
	}

	public function update($id){
		$data = $this->Pengumumanmodel->detail($id);
		
		// Validasi
		$valid = $this->form_validation;
		$valid->set_rules('judul','judul','required',
			array( 'required' => 'judul harus diisi'));

		if($valid->run()===FALSE) {
		// End validasi

			$data = array(
				'data'	=> $data,
				'isi' 	=> 'pengumuman/update');
			$this->load->view('layout/wrapper',$data);
			$role = $this->session->userdata('roleid');
			if ($role != 1) {
				redirect(base_url('login/logout'));
			}
		// masuk database
		}else{
			$i = $this->input;
			$data = array( 
				'judul' => $i->post('judul'),
				'konten' => $i->post('pengumuman'),
				'tanggal_post' => date('Y-m-d H:i:s'));
			$this->Pengumumanmodel->edit($data,$id);
			$this->session->set_flashdata('sukses','Data Berhasil Dirubah');
			redirect(base_url('pages/pengumuman'));
		}
	}
	public function delete($id) {
		$this->simple_login->cek_login();
		$role = $this->session->userdata('roleid');
		if ($role == 1) {
			$data = array('id_pengumuman'	=> $id);
			$this->Pengumumanmodel->delete($data);
			$this->session->set_flashdata('sukses','Data telah dihapus');
			redirect(base_url('pages/pengumuman'));	
		}else{
			redirect(base_url('login/logout'));
		}
	}
}
?>