<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('usermodel');
		$this->load->library('Simple_login');
		$this->load->library('session');
	}

	public function index()
	{
		$user = $this->usermodel->getAll();
		$data = array(
			'user' => $user,
			'isi' => 'user/list'
		);
		$this->load->view('layout/wrapper',$data);
		$role = $this->session->userdata('roleid');
		if ($role != 1) {
			redirect(base_url('userdata'));
		}

	}

	public function delete($id_user){
		$role = $this->session->userdata('roleid');
		if ($role == 1) {
			$data = array('id'	=> $id_user);
			$this->usermodel->delete($data);
			$this->session->set_flashdata('sukses','Data telah dihapus');
			redirect(base_url('pages/user'));	
		}else{
			redirect(base_url('login/logout'));
		}
	}

	public function update($id_user){
		$user = $this->usermodel->detail($id_user);
		
		// Validasi
		$valid = $this->form_validation;
		$valid->set_rules('nama','Nama','required',
			array( 'required' => 'Nama harus diisi'));

		if($valid->run()===FALSE) {
		// End validasi

			$data = array(
				'user'	=> $user,
				'isi' 	=> 'user/edit');
			$this->load->view('layout/wrapper',$data);
		// masuk database
		}else{
			$role = $this->session->userdata('roleid');
			$username = $this->session->userdata('username');
			$i = $this->input;
			if ($role == 1) {
				$data = array( 	
					'username' => $i->post('username'),
					'password' => $i->post('password'),
					'nama' => $i->post('nama'),
					'alamat' => $i->post('alamat'),
					'roleid' => $i->post('roleid'),
					'jabatan' => $i->post('jabatan'),
					'kelamin' => $i->post('kelamin'),
					'telepon' => $i->post('telepon'));
				$this->usermodel->edit($data,$id_user);
				$this->session->set_flashdata('sukses','Data User Berhasil Dirubah');
				redirect(base_url('pages/user'));
			}else{
				$data = array( 		
					'username' => $username,
					'password' => $i->post('password'),
					'nama' => $i->post('nama'),
					'alamat' => $i->post('alamat'),
					'roleid' => $i->post('roleid'),
					'jabatan' => $i->post('jabatan'),
					'kelamin' => $i->post('kelamin'),
					'telepon' => $i->post('telepon'));
				$this->usermodel->edit($data,$id_user);
				$this->session->set_flashdata('sukses','Data User Berhasil Dirubah');
				redirect(base_url('userdata'));
			}
		}
	}
	public function insert(){
		$valid = $this->form_validation;

		$valid->set_rules('username','Username','required|is_unique[tb_user.username]',
			array( 	'required' 	=> 'Username harus diisi',
				'is_unique'	=> 'Username: <strong>'.$this->input->post('username').
				'</strong> sudah digunakan. Buat username baru!'));
		
		$valid->set_rules('password','Password','required',
			array( 'required' => 'Password harus diisi'));
		
		if($valid->run()===FALSE) {
		// End validasi

			$data = array('isi'  => 'user/insert');
			$this->load->view('layout/wrapper',$data);
		// masuk database
		}else{
			$i = $this->input;
			$data = array( 
				'username' => $i->post('username'),
				'password' => $i->post('password'),
				'nama' => $i->post('nama'),
				'alamat' => $i->post('alamat'),
				'roleid' => $i->post('roleid'),
				'jabatan' => $i->post('jabatan'),
				'kelamin' => $i->post('kelamin'),
				'telepon' => $i->post('telepon'));
			$this->usermodel->insert($data);
			$this->session->set_flashdata('sukses','user telah ditambah');
			redirect(base_url('pages/user'));
			$role = $this->session->userdata('roleid');
			if ($role != 1) {
				redirect(base_url('login/logout'));
			}
		}
	}

	public function user(){
		$this->load->view('user/home');
	}
}
