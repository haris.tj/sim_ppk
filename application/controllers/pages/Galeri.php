<?php 
/**
 * 
 */
class Galeri extends CI_Controller
{
	
	public function __construct() {
		parent::__construct();
		$this->load->model('galerimodel');
		$this->load->library('Simple_login');
		$this->load->library('session');
	}

	public function index()
	{
		$x = $this->galerimodel->getAll();
		$data = array(
			'galeri' => $x,
			'isi' => 'galeri/list'
		);
		$this->load->view('layout/wrapper',$data);

	}
	public function delete($id){
		$role = $this->session->userdata('roleid');
		if ($role == 1) {
			$data = array('id_gambar'	=> $id);
			$this->galerimodel->delete($data);
			$this->session->set_flashdata('sukses','Data telah dihapus');
			redirect(base_url('pages/galeri'));	
		}else{
			redirect(base_url('login/logout'));
		}
	}
	public function insert(){
		// Validasi
		$v = $this->form_validation;
		
		$v->set_rules('judul','Judul Gambar','required|is_unique[tb_galeri.judul]',
			array(	'required'		=> 'Judul Tidak Boleh Kosong',
				'is_unique'		=> 'Judul : <strong>'.$this->input->post('judul').
				'</strong> sudah ada. Buat Judul yang berbeda'));
		
		if($v->run()) {
			$config['upload_path'] 		= './assets/upload/image/';
			$config['allowed_types'] 	= 'gif|jpg|png|svg';
			$config['max_size']			= '12000'; // KB	
			$this->load->library('upload', $config);
			if(! $this->upload->do_upload('gambar')) {
		// End validasi

				$data = array(
					'isi'		=> 'galeri/insert');
				$this->load->view('layout/wrapper', $data);
		// Masuk database
			}else{
				$upload_data				= array('uploads' =>$this->upload->data());
			// Image Editor
				$config['image_library']	= 'gd2';
				$config['source_image'] 	= './assets/upload/image/'.$upload_data['uploads']['file_name']; 
				$config['new_image'] 		= './assets/upload/image/thumbs/';
				$config['create_thumb'] 	= TRUE;
				$config['quality'] 			= "100%";
				$config['maintain_ratio'] 	= TRUE;
			$config['width'] 			= 360; // Pixel
			$config['height'] 			= 200; // Pixel
			$config['x_axis'] 			= 0;
			$config['y_axis'] 			= 0;
			$config['thumb_marker'] 	= '';
			$this->load->library('image_lib', $config);
			$this->image_lib->resize();
			
			// Proses ke database
			$i = $this->input;
			$data = array(
				'gambar'				=> $upload_data['uploads']['file_name'],
				'judul'					=>  $i->post('judul'),
				'keterangan'			=>  $i->post('keterangan'));
			$this->galerimodel->insert($data);
			$this->session->set_flashdata('sukses','Gambar Berhasil Ditambah');
			redirect(base_url('pages/galeri'));
		}}
		// End masuk database
		$data = array(
			'isi'		=> 'galeri/insert');
		$this->load->view('layout/wrapper', $data);
		$role = $this->session->userdata('roleid');
		if ($role != 1) {
			redirect(base_url('login/logout'));
		}
	}

	public function update($idgambar){
		$gambar		= $this->galerimodel->detail($idgambar);
		// Validasi
		$v = $this->form_validation;
		
		$v->set_rules('keterangan','Keterangan berita','required',
			array(	'required'		=> 'Keterangan berita harus diisi'));
		
		if($v->run()) {
			if(!empty($_FILES['gambar']['name'])) {
				$config['upload_path'] 		= './assets/upload/image/';
				$config['allowed_types'] 	= 'gif|jpg|png|svg';
			$config['max_size']			= '12000'; // KB	
			$this->load->library('upload', $config);
			if(! $this->upload->do_upload('gambar')) {
		// End validasi

				$data = array(	
					'data'					=> $gambar,
					'gambar'				=> $upload_data['uploads']['file_name'],
					'judul'					=>  $i->post('judul'),
					'keterangan'			=>  $i->post('keterangan'),
					'isi'					=>  'galeri/update');
				$this->load->view('layout/wrapper', $data);
		// Masuk database
			}else{
				$upload_data				= array('uploads' =>$this->upload->data());
			// Image Editor
				$config['image_library']	= 'gd2';
				$config['source_image'] 	= './assets/upload/image/'.$upload_data['uploads']['file_name']; 
				$config['new_image'] 		= './assets/upload/image/thumbs/';
				$config['create_thumb'] 	= TRUE;
				$config['quality'] 			= "100%";
				$config['maintain_ratio'] 	= TRUE;
			$config['width'] 			= 360; // Pixel
			$config['height'] 			= 200; // Pixel
			$config['x_axis'] 			= 0;
			$config['y_axis'] 			= 0;
			$config['thumb_marker'] 	= '';
			$this->load->library('image_lib', $config);
			$this->image_lib->resize();
			
			// Proses ke database
			$i = $this->input;
			$data = array(	
				'id_gambar'				=> $idgambar,
				'gambar'				=> $upload_data['uploads']['file_name'],
				'judul'					=> $i->post('judul'),
				'keterangan'			=> $i->post('keterangan'));

			$this->galerimodel->edit($data);
			$this->session->set_flashdata('sukses','Berita telah diedit');
			redirect(base_url('pages/galeri'));
		}}else{
			// Proses ke database
			$i = $this->input;
			$data = array(	
				'id_gambar'				=> $idgambar,
				'gambar'				=> $upload_data['uploads']['file_name'],
				'judul'					=> $i->post('judul'),
				'keterangan'			=> $i->post('keterangan'));
			$this->galerimodel->edit($data);
			$this->session->set_flashdata('sukses','Berita telah diedit');
			redirect(base_url('pages/galeri'));
		}}
		// End masuk database
		$data = array(
			'data'	=> $gambar,
			'isi'		=> 'galeri/update'); 
		$this->load->view('layout/wrapper', $data);
		$role = $this->session->userdata('roleid');
		if ($role != 1) {
			redirect(base_url('login/logout'));
		}
	}




}
?>