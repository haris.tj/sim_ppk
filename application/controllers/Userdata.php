<?php  

/**
 * 
 */
class UserData extends CI_Controller
{
	function __construct(){
		parent::__construct();
		$this->load->model('loginmodel');
	}
	public function index()
	{
		$user = $this->loginmodel->getDetail();
		$data = array(
			'user' => $user,
			'isi' => 'user/profile'
		);
		$this->load->view('layout/wrapper',$data);
	}
}
?>