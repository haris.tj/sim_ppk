<?php   
$this->load->library('Simple_login');
$this->load->library('session'); 
$role = $this->session->userdata('roleid');
$name = $this->session->userdata('username');
?>
<div class="wrapper">
    <div class="sidebar" data-color="purple" data-image="<?php echo base_url('assets/img/sidebar-5.jpg') ?>">
       <div class="sidebar-wrapper">
        <div class="logo">
            <a href="#" class="simple-text">
             Gereja Kalvari Pentakosta
         </a>
     </div>

     <ul class="nav">
        <li>
            <a href="<?php echo base_url('userdata') ?>">
                <i class="pe-7s-user"></i>
                <p><?php echo $name ?> Profile</p>
            </a>
        </li>

        <?php 
        if ($role == 1) { ?>
            <li>
                <a href="<?php echo base_url('pages/user') ?>">
                    <i class="pe-7s-note2"></i>
                    <p>User</p>
                </a>
            </li>
        <?php } ?>
        <li>
            <a href="<?php echo base_url('pages/pengumuman') ?>">
                <i class="pe-7s-news-paper"></i>
                <p>Pengumuman</p>
            </a>
        </li>
        <li>
            <a href="<?php echo base_url('pages/galeri') ?>">
                <i class="pe-7s-science"></i>
                <p>Galeri</p>
            </a>
        </li>
        <li>
            <a href="<?php echo base_url('pages/program') ?>">
                <i class="pe-7s-map-marker"></i>
                <p>Rencana Program</p>
            </a>
        </li>
        <li>
            <a href="<?php echo base_url('pages/Realisasi') ?>">
                <i class="pe-7s-map-marker"></i>
                <p>Realisasi</p>
            </a>
        </li>

    </ul>
</div>
</div>