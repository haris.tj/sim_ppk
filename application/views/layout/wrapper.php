<?php 
$this->load->library('Simple_login');
$this->load->library('session');
$role =  $this->session->userdata('roleid');

if ($role == '') {
	redirect(base_url('login'));
}else{
	$this->load->view('layout/header');
	$this->load->view('layout/sidebar');
	$this->load->view('layout/main');
	$this->load->view('layout/footer');	
}
?>