 <h4 class="title"> <strong>Table Program</strong></h4>
 <?php
// Notifikasi
 if($this->session->flashdata('sukses')) {
 	echo '<div class="alert alert-success">';
 	echo $this->session->flashdata('sukses');
 	echo '</div>';
 }
 echo validation_errors('<div class="alert alert-success">','</div>');
 $this->load->library('Simple_login');
 $this->load->library('session');
 ?>

 <div class="col-md-12">
 	<div class="card">
 		<div class="header">
 			<a href="<?php echo base_url('pages/program/insert') ?>" class="btn btn-info btn-fill btn-sm">Add Program</a>
 		</div>
 		<div class="content table-responsive table-full-width">
 			<table class="table table-hover table-striped" id="datatable">
 				<thead>
 					<th>No</th>
 					<th>Nama Program</th>
 					<th>Deskripsi</th>
 					<th>Periode</th>
 					<th>Waktu Pelaksanaan</th>
 					<th>Pelaksanaan</th>
 					<th>Tempat</th>
 					<th>Anggaran</th>
 					<th>Action</th>
 				</thead>
 				<tbody>
 					<?php $i=1; foreach ($user as $dt) { ?>
 						<tr>
 							<td><?php echo $i ?></td>
 							<td><?php echo $dt->nama_program ?></td>
 								<td><?php echo $dt->deskripsi ?></td>
 								<td><?php echo $dt->periode ?></td>
 								<td><?php echo $dt->waktu ?></td>
 								<td><?php echo $dt->pelaksanaan ?></td>
 								<td><?php echo $dt->tempat ?></td>
 								<td><?php echo $dt->anggaran ?></td>
 								<td>
 									<a href="<?php echo base_url('pages/program/realis/'.$dt->id_program) ?>" class="btn btn-fill btn-success btn-sm">ok</a>
 									<a href="<?php echo base_url('pages/program/update/'.$dt->id_program) ?>" class="btn btn-warning btn-fill btn-sm">Update</a>
 									<?php include('delete.php') ?>
 								</td>
 							</tr>
 							<?php  $i++; } ?>
 						</tbody>
 					</table>

 				</div>
 			</div>
 		</div>