<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.15/tinymce.min.js" referrerpolicy="origin"></script>
<script type="text/javascript">
    tinymce.init({selector:'textarea'});
</script>


<?php
// Error
if(isset($error)) {
    echo '<div class="alert alert-warning">';
    echo $error;
    echo '</div>';
}

// Validasi
echo validation_errors('<div class="alert alert-success">','</div>');

// Form
echo form_open_multipart('pages/program/insert');
?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-14">
                <div class="card">
                    <div class="header">
                        <h4 class="title"><strong>Insert Rencana Program</strong></h4>
                    </div>
                    <div class="content">
                        <form>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Nama Program</label>
                                        <input type="text" class="form-control" placeholder="Nama Program" value="" name="nama_program">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Tempat</label>
                                        <input type="text" class="form-control" placeholder="Tempat" value="" name="tempat" id="tempat">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Periode</label>
                                        <input type="text" class="form-control" placeholder="Periode" name="periode" id="periode" value=""> 
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Waktu</label>
                                        <input type="date" class="form-control" placeholder=""  name="waktu" value="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                     <label>Pelaksanaan</label>
                                     <input type="text" class="form-control" placeholder="Pelaksanaan"  name="pelaksanaan" value="">
                                 </div>
                             </div>
                             <div class="col-md-4">
                                <div class="form-group">
                                    <label>Anggaran</label>
                                    <input type="number" class="form-control" placeholder="Anggaran" name="anggaran" value="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Deskripsi</label>
                                <textarea name="deskripsi" class="form-control" rows="15" placeholder="Keterangan atau spesifikasi…"></textarea>
                            </div>

                            <div class="form-group">
                                <input type="submit" name="submit" value="Save" class="btn btn-primary">
                                <input type="reset" name="reset" value="Clear" class="btn btn-danger">

                            </div>    </div>

                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>


    </div>
</div>
</div>
<?php echo form_close() ?>