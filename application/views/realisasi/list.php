 <h4 class="title"> <strong>Table User</strong></h4>
 <?php
// Notifikasi
 if($this->session->flashdata('sukses')) {
  echo '<div class="alert alert-success">';
  echo $this->session->flashdata('sukses');
  echo '</div>';
}

// Error
echo validation_errors('<div class="alert alert-success">','</div>');
?>

<div class="col-md-12">
  <div class="card">
    <div class="header">
      <a href="<?php echo base_url('pages/program/') ?>" class="btn btn-info btn-fill btn-sm">Add Realisasi</a>
      <a href="<?php echo base_url('DownloadPdf') ?>" class="btn btn-info btn-fill btn-sm" target="_blank">Download Pdf</a>
    </div>
    <div class="content table-responsive table-full-width">
      <table class="table table-hover table-striped" id="datatable">
        <thead>
          <th>No</th>
          <th>Realisasi</th>
          <th>Deskripsi</th>
          <th>Periode</th>
          <th>Waktu</th>
          <th>Pelaksanaan</th>
          <th>Tempat</th>
          <th>Anggaran</th>
          <th>Laporan</th>
          <th>Action</th>
        </thead>
        <tbody>
          <?php $i=1; foreach ($realis as $dt) { ?>
            <tr>
             <td><?php echo $i ?></td>
             <td><?php echo $dt->nama_realisasi ?></td>
             <td><?php echo $dt->deskripsi ?></td>
             <td><?php echo $dt->periode ?></td>
             <td><?php echo $dt->waktu ?></td>
             <td><?php echo $dt->pelaksanaan ?></td>
             <td><?php echo $dt->tempat ?></td>
             <td><?php echo $dt->anggaran ?></td>
             <td><a href="<?php echo base_url('./assets/upload/files/'.$dt->laporan) ?>" target="_blank"><?php echo $dt->laporan ?></a></td>
             <td>

               <button id="btn-realisasi" name="<?php echo $dt->id_realisasi ?>" class="btn btn-primary btn-fill btn-sm">Realisasi</button>




               <a href="<?php echo base_url('pages/realisasi/update/'.$dt->id_realisasi) ?>" class="btn btn-warning btn-fill btn-sm">Edit</a>
               <?php include('delete.php') ?>
             </td>
           </tr>
           <?php  $i++; } ?>
         </tbody>
       </table>
       <script  src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
       <script type="text/javascript">
        $("#btn-realisasi").click(function(){
         var x = document.getElementsByName(<?php echo $dt->id_realisasi ?>);
         alert(x);
       });
     </script>
   </div>
 </div>
</div>