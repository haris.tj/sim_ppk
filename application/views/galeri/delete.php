
<style type="text/css">
   body .modal-backdrop.fade.in {
    z-index: 0 !important;
}
</style>
<button class="btn btn-primary btn-fill btn-sm" data-toggle="modal" data-target="#Delete<?php echo $dt->id_gambar ?>">Delete
</button>
<div class="modal fade" id="Delete<?php echo $dt->id_gambar ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Hapus  Data</h4>
            </div>
            <div class="modal-body">

                <p class="alert alert-success">Yakin ingin menghapus data ini?</p>

            </div>
            <div class="modal-footer">

                <a href="<?php echo base_url('pages/galeri/delete/'.$dt->id_gambar) ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i> Ya, Hapus</a>
                
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
