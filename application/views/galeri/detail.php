<!-- Button trigger modal -->
<style type="text/css">
  .center {
    display: block;
    margin-left: auto;
    margin-right: auto;
  }
</style>
<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal">
 Detail
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel" style="text-align: center;"><?php echo $dt->judul ?></h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
        <img src="<?php echo base_url('assets/upload/image/thumbs/'.$dt->gambar) ?>" class="img img-responsive center"><br>
        <?php echo $dt->keterangan ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>