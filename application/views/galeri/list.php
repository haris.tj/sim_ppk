 <h4 class="title"> <strong>Table Galeri</strong></h4>
 <?php
// Notifikasi
 if($this->session->flashdata('sukses')) {
    echo '<div class="alert alert-success">';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}
echo validation_errors('<div class="alert alert-success">','</div>');
$this->load->library('Simple_login');
$this->load->library('session');
?>

<div class="col-md-12">
    <div class="card">

        <?php $role =  $this->session->userdata('roleid');
        if ($role == 1){ ?>
            <div class="header">
                <a href="<?php echo base_url('pages/galeri/insert') ?>" class="btn btn-info btn-fill btn-sm">Add Galeri</a>
            </div>
        <?php } ?>
        <div class="content table-responsive table-full-width">
            <table class="table table-hover table-striped" id="datatable">
             <thead>
                <th>No</th>
                <th>Gambar</th>
                <th>Judul</th>
                <?php $role =  $this->session->userdata('roleid');
                if ($role == 1){ ?>
                    <th>Action</th>
                <?php } ?>
            </thead>
            <tbody>
                <?php $i=1; foreach ($galeri as $dt) { ?>
                    <tr>
                     <td><?php echo $i ?></td>
                     <td>
                        <img src="<?php echo base_url('assets/upload/image/thumbs/'.$dt->gambar) ?>" class="img img-responsive" width="60"></td>
                        <td><?php echo $dt->judul ?></td>
                        <?php 
                        $role =  $this->session->userdata('roleid');
                        if ($role == 1){ ?>
                            <td>
                               <?php include('detail.php') ?>
                             <a href="<?php echo base_url('pages/galeri/update/'.$dt->id_gambar) ?>" class="btn btn-warning btn-fill btn-sm">Update</a>
                             <?php include('delete.php') ?>
                         <?php } ?>
                     </td>
                 </tr>
                 <script type="text/javascript">
                   function klik(){
                    swal("Keterangan", "Ini Adalah Keterangan", "success");
                }
            </script>
            <?php  $i++; } ?>
        </tbody>
    </table>

</div>
</div>
</div>