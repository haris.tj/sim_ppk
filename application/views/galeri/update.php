<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.15/tinymce.min.js" referrerpolicy="origin"></script>
<script type="text/javascript">
    tinymce.init({selector:'textarea'});
</script>

<?php
// Error
if(isset($error)) {
	echo '<div class="alert alert-warning">';
	echo $error;
	echo '</div>';
}

// Validasi
echo validation_errors('<div class="alert alert-success">','</div>');

// Form
echo form_open_multipart('pages/galeri/update/'.$data->id_gambar);
?>

<h4><strong>Update Image</strong></h4>
<div class="card"></div>
<div class="card-header">

</div>
<div class="col-md-6">
    <div class="form-group">
        <label>Judul Gambar</label>
        <input type="text" name="judul" placeholder="Judul Gambar" required class="form-control" value="<?php echo $data->judul ?>">
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
       <label>Upload gambar</label>
       <input type="file" name="gambar" class="form-control">
   </div>
</div>

<div class="col-md-12">
    <div class="form-group">
        <label>Keterangan</label>
        <textarea name="keterangan" class="form-control" rows="15" placeholder="Keterangan atau spesifikasi…"><?php echo $data->keterangan ?></textarea>
    </div>

    <div class="form-group">
        <input type="submit" name="submit" value="Save" class="btn btn-primary">
        <input type="reset" name="reset" value="Clear" class="btn btn-danger">

    </div>    </div>

</div>
<?php echo form_close() ?>

