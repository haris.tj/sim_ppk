<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.15/tinymce.min.js" referrerpolicy="origin"></script>
<script type="text/javascript">
    tinymce.init({selector:'textarea'});
</script>

<?php
// Error
if(isset($error)) {
	echo '<div class="alert alert-warning">';
	echo $error;
	echo '</div>';
}

// Validasi
echo validation_errors('<div class="alert alert-success">','</div>');

// Form
echo form_open_multipart('pages/pengumuman/insert');
?>

<h4><strong>Insert Image</strong></h4>
<div class="card"></div>
<div class="card-header">

</div>
<div class="col-md-12">
    <div class="form-group">
        <label>Judul Pengumuman</label>
        <input type="text" name="judul" placeholder="Judul Pengumuman" required class="form-control">
    </div>
</div>
<div class="col-md-12">
    <div class="form-group">
        <label>Pengumuman</label>
        <textarea name="pengumuman" class="form-control" rows="15" placeholder="Keterangan atau spesifikasi…"></textarea>
    </div>

    <div class="form-group">
        <input type="submit" name="submit" value="Save" class="btn btn-primary">
        <input type="reset" name="reset" value="Clear" class="btn btn-danger">

    </div>    </div>

</div>
<?php echo form_close() ?>

