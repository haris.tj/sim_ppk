 <h4 class="title"> <strong>Table List User</strong></h4>
 <?php
// Notifikasi
 if($this->session->flashdata('sukses')) {
  echo '<div class="alert alert-success">';
  echo $this->session->flashdata('sukses');
  echo '</div>';
}

// Error
echo validation_errors('<div class="alert alert-success">','</div>');
$this->load->library('Simple_login');
$this->load->library('session');
$role =  $this->session->userdata('roleid');
?>

<div class="col-md-12">
  <div class="card">
    <div class="header">
      <?php if ($role == 1){ ?>
        <a href="<?php echo base_url('pages/pengumuman/insert') ?>" class="btn btn-info btn-fill btn-sm">Add Pengumuman</a>
      <?php } ?>
    </div>
    <div class="content table-responsive table-full-width">
      <table class="table table-hover table-striped" id="datatable">
        <thead>
          <th>No</th>
          <th>Judul</th>
          <th>Pengumuman</th>
          <th>Tanggal Post</th>
          <?php if ($role == 1){ ?>
            <th>Action</th>
          <?php } ?>
        </thead>
        <tbody>
          <?php $i=1; foreach ($pengumuman as $dt) { ?>
            <tr>
             <td><?php echo $i ?></td>
             <td><?php echo $dt->judul ?></td>
             <td><?php echo $dt->konten ?></td>
             <td><?php echo $dt->tanggal_post ?></td>
             <?php if ($role == 1){ ?>
               <td>
                <a href="#" class="btn btn-success btn-fill btn-sm">Detail</a>
                <a href="<?php echo base_url('pages/pengumuman/update/'.$dt->id_pengumuman) ?>" class="btn btn-warning btn-fill btn-sm">Update</a>
                <?php include('delete.php') ?>
              </td>
            <?php } ?>
          </tr>
          <?php  $i++; } ?>
        </tbody>
      </table>

    </div>
  </div>
</div>