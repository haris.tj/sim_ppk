<?php 
// cetak error kalau ada salah input
echo validation_errors('<div class="alert alert-warning">','</div>');

echo form_open(base_url('pages/user/update/'.$user->id));
$this->load->library('Simple_login');
$this->load->library('session');
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-14">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Edit Profile</h4>
                    </div>
                    <div class="content">
                        <form>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Username</label>
                                        <?php $role =  $this->session->userdata('roleid');
                                        if ($role == 1){ ?>
                                            <input type="text" class="form-control" placeholder="Username" value="<?php echo $user->username ?>" name="username">
                                        <?php }else{ ?>
                                            <input type="text" class="form-control" placeholder="Username" disabled value="<?php echo $user->username ?>" name="username">
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="text" class="form-control" placeholder="Password" value="<?php echo $user->password ?>" name="password" id="Password">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Nama</label>
                                        <input type="text" class="form-control" placeholder="Nama" name="nama" id="nama" value="<?php echo $user->nama ?>"> 
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label>Alamat</label>
                                        <input type="text" class="form-control" placeholder="Home Address"  name="alamat" value="<?php echo $user->alamat ?>">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">

                                       <?php $role =  $this->session->userdata('roleid');
                                       if ($role == 1){ ?>
                                           <label>Role</label>
                                           <select name="roleid" class="form-control">
                                            <option value="">-----</option>
                                            <option value="1">Admin</option>
                                            <option value="2">User</option>
                                        </select>
                                    <?php }else{ ?>
                                        <input type="hidden" class="form-control" placeholder="Home Address"  name="roleid" value="2"><?php } ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Jabatan</label>
                                        <input type="text" class="form-control" placeholder="Jabatan"  name="jabatan" value="<?php echo $user->jabatan ?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                     <label>Kelamin</label>
                                     <select name="kelamin" class="form-control">
                                        <option value="">-----</option>
                                        <option value="L">Laki-Laki</option>
                                        <option value="P">Perempuan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Telepon</label>
                                    <input type="number" class="form-control" placeholder="Telepon" name="telepon" value="<?php echo $user->telepon ?>">
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-info btn-fill pull-right">Update Profile</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>


    </div>
</div>
</div>

<?php echo form_close() ?>