 <h4 class="title"> <strong>Table User</strong></h4>
 <?php
// Notifikasi
 if($this->session->flashdata('sukses')) {
  echo '<div class="alert alert-success">';
  echo $this->session->flashdata('sukses');
  echo '</div>';
}

// Error
echo validation_errors('<div class="alert alert-success">','</div>');
?>

<div class="col-md-12">
  <div class="card">
    <div class="header">
      <a href="<?php echo base_url('pages/user/insert') ?>" class="btn btn-info btn-fill btn-sm">Add User</a>
    </div>
    <div class="content table-responsive table-full-width">
      <table class="table table-hover table-striped" id="datatable">
        <thead>
          <th>No</th>
          <th>Username</th>
          <th>Nama</th>
          <th>Jabatan</th>
          <th>Alamat</th>
          <th>Kelamin</th>
          <th>Telepon</th>
          <th>Role</th>
          <th>Action</th>
        </thead>
        <tbody>
          <?php $i=1; foreach ($user as $dt) { 
            $role = $dt->roleid;
            $kelamin = $dt->kelamin;
            if ($role ==1) {
              $role = "Admin";
            }else{
              $role = "User";
            }
            if ($kelamin == "L") {
              $kelamin = "Laki-Laki";
            }else{
              $kelamin = "Perempuan";
            }

            ?>
            <tr>
             <td><?php echo $i ?></td>
             <td><?php echo $dt->username ?></td>
             <td><?php echo $dt->nama ?></td>
             <td><?php echo $dt->jabatan ?></td>
             <td><?php echo $dt->alamat ?></td>
             <td><?php echo $kelamin ?></td>
             <td><?php echo $dt->telepon ?></td>
             <td><?php echo $role ?></td>
             <td>
              <a href="<?php echo base_url('pages/user/update/'.$dt->id)?>" class="btn btn-warning btn-fill btn-sm">Update</a>
              <?php include('delete.php'); ?>
            </td>
          </tr>
          <?php  $i++; } ?>
        </tbody>
      </table>

    </div>
  </div>
</div>