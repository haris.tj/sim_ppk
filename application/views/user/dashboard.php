<div class="row">
    <!-- Berita -->
    <div class="col-md-4 col-sm-6 col-xs-6">           
        <div class="panel panel-back noti-box">
            <span class="icon-box bg-color-red set-icon">
                <i class="fa fa-newspaper-o fa-5x"></i>
            </span>
            <div class="text-box" >
                 <p class="main-text"><h3 style="text-align: center;"><strong><?php echo $user ?></strong></h3></p>
                <p class="text-muted"style="text-align: center;"><a href="<?php echo base_url('pages/user') ?>">User</a></p>
            </div>
        </div>
    </div>

    <!-- Produk -->
    <div class="col-md-4 col-sm-6 col-xs-6">           
        <div class="panel panel-back noti-box">
            <span class="icon-box bg-color-red set-icon">
                <i class="fa fa-book fa-5x"></i>
            </span>
            <div class="text-box" >
                <p class="main-text"><h3 style="text-align: center;"><strong><?php echo $galeri ?></strong></h3></p>
                <p class="text-muted" style="text-align: center;"><a href="<?php echo base_url('pages/galeri') ?>">Galeri</a></p>
            </div>
        </div>
    </div>

    <!-- Video -->
    <div class="col-md-4 col-sm-6 col-xs-6">           
        <div class="panel panel-back noti-box">
            <span class="icon-box bg-color-red set-icon">
                <i class="fa fa-film fa-5x"></i>
            </span>
            <div class="text-box" >
                 <p class="main-text"><h3 style="text-align: center;"><strong><?php echo $program ?></strong></h3></p>
                <p class="text-muted"style="text-align: center;"><a href="<?php echo base_url('pages/program') ?>">Program</a></p>
            </div>
        </div>
    </div>

   



</div>