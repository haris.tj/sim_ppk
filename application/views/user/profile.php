<?php 
$kelamin = $user->kelamin;
$role = $user->roleid;
if ($kelamin == "L") {
    $kelamin = "Laki-Laki";
}elseif ($kelamin == "P") {
    $kelamin = "Perempuan";
} if ($role == 1) {
    $role = "Admin";
}elseif ($role == 2) {
    $role = "User";
}

?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-14">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Profile</h4>
                    </div>
                    <div class="content">
                        <form>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input type="text" class="form-control"  disabled placeholder="Username" value="<?php echo $user->username ?>" name="username">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="text" class="form-control"  disabled placeholder="Password" value="<?php echo $user->password ?>" name="password" id="Password">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Nama</label>
                                        <input type="text" class="form-control"  disabled placeholder="Nama" name="nama" id="nama" value="<?php echo $user->nama ?>"> 
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label>Alamat</label>
                                        <input type="text" class="form-control"  disabled placeholder="Home Address"  name="alamat" value="<?php echo $user->alamat ?>">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                     <label>Role</label>
                                     <input type="text" class="form-control"  disabled placeholder="Home Address"  name="roleid" value="<?php echo $role ?>">
                                 </div>
                             </div>
                         </div>

                         <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Jabatan</label>
                                    <input type="text" class="form-control"  disabled placeholder="Jabatan"  name="jabatan" value="<?php echo $user->jabatan ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                   <label>Kelamin</label>
                                   <input type="text" class="form-control"  disabled placeholder="Jenis Kelamin"  name="kelamin" value="<?php echo $kelamin ?>">
                               </div>
                           </div>
                           <div class="col-md-4">
                            <div class="form-group">
                                <label>Telepon</label>
                                <input type="number" class="form-control"  disabled placeholder="Telepon" name="telepon" value="<?php echo $user->telepon ?>">
                            </div>
                        </div>
                    </div>
                    <a href="<?php echo base_url('pages/user/update/'.$user->id) ?>" class="btn btn-info btn-fill btn-sm pull-right">Update Profile</a>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>


</div>
</div>
</div>
