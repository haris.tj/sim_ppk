<!DOCTYPE html>
<html lang="en">
<head>
  <link rel="icon" type="image/png" href="<?php echo base_url().'assets2/images/kalvari/pantekosta.jpg'?>" />
  <title>Gereja Kalvari Pentakosta Jemaat Getsemani Sorong</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">

  <link rel="stylesheet" href="<?php echo base_url().'assets2/css/open-iconic-bootstrap.min.css'?>">
  <link rel="stylesheet" href="<?php echo base_url().'assets2/css/animate.css'?>">

  <link rel="stylesheet" href="<?php echo base_url().'assets2/css/owl.carousel.min.css'?>">
  <link rel="stylesheet" href="<?php echo base_url().'assets2/css/owl.theme.default.min.css'?>">
  <link rel="stylesheet" href="<?php echo base_url().'assets2/css/magnific-popup.css'?>">

  <link rel="stylesheet" href="<?php echo base_url().'assets2/css/aos.css'?>">

  <link rel="stylesheet" href="<?php echo base_url().'assets2/css/ionicons.min.css'?>">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.css">


  <link rel="stylesheet" href="<?php echo base_url().'assets2/css/flaticon.css'?>">
  <link rel="stylesheet" href="<?php echo base_url().'assets2/css/icomoon.css'?>">
  <link rel="stylesheet" href="<?php echo base_url().'assets2/css/style.css'?>">
</head>
<body>

  <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
      <!-- <a class="navbar-brand" href="index.html">Even<span>talk.</span></a> -->
      <!-- <div class="img speaker-img" style="background-image: url(assets2/images/logo_untad.png);"></div> -->
      <!-- <img src="assets2/images/kalvari/pantekosta.png" width="200px" style="padding-top: 5px;"/> -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="oi oi-menu"></span> Menu
      </button>

      <div class="collapse navbar-collapse" id="ftco-nav">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active"><a href="<?=base_url('')?>" class="nav-link"><b>Home</b></a></li>
          <!-- <li class="nav-item"><a href="about.html" class="nav-link">About</a></li>
          <li class="nav-item"><a href="speakers.html" class="nav-link">Speakers</a></li>
          <li class="nav-item"><a href="schedule.html" class="nav-link">Schedule</a></li>
          <li class="nav-item"><a href="blog.html" class="nav-link">Blog</a></li>
          <li class="nav-item"><a href="contact.html" class="nav-link">Contact</a></li> -->
          <li class="nav-item cta mr-md-2"><a href="<?=base_url('login')?>" class="nav-link">Login</a></li>

        </ul>
      </div>
    </div>
  </nav>
  <!-- END nav -->

  <div class="hero-wrap js-fullheight" style="background-image: url('assets2/images/kalvari/merpati.png');" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
      <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-start" data-scrollax-parent="true">
        <div class="col-xl-10 ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
          <h2 class="mb-4" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">Sistem Informasi Perencanaan Program Kerja Jemaat Gereja Kalvari Pentakosta Jemaat Getsemani Sorong,Papua Barat</h2>
          <!-- <p class="mb-4" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">December 21-24, 2019. Paris, Italy</p> -->
          <div id="timer" class="d-flex mb-3">
            <div class="time" id="days"></div>
            <div class="time pl-4" id="hours"></div>
            <div class="time pl-4" id="minutes"></div>
            <div class="time pl-4" id="seconds"></div>
          </div>
        </div>
      </div>
    </div>
  </div>



  <section class="ftco-counter img" id="section-counter">
  <h1><center>Pengumuman / Berita</center> </h1>
  <br>
    <div class="container">
      <div class="row d-flex">
          <?php for ($i= 0; $i<=2; $i++): ?>
            <div class="col-md-4 d-flex">
              <div class="card">
                <img style="height: 40%;" src="<?php echo base_url().'assets2/images/kalvari/no_image.png'?>" class="card-img-top" alt="nama-gambar">
                <div class="card-body">
                  <h5 class="card-title">Judul Pengumuman</h5>
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                  <p class="card-text"><small class="text-muted">Di Update 3 menit yang lalu</small></p>
                </div>

              </div>
            </div>
          <?php endfor; ?>

      </div>
    </div>
<br>
  </section>


  <!-- <section class="ftco-section bg-light">
  <div class="container">
  <div class="row justify-content-center mb-5 pb-3">
  <div class="col-md-7 heading-section text-center ftco-animate">
  <span class="subheading">Our Blog</span>
  <h2><span>Recent</span> Blog</h2>
</div>
</div>
<div class="row d-flex">
<div class="col-md-4 d-flex ftco-animate">
<div class="blog-entry justify-content-end">
<a href="blog-single.html" class="block-20" style="background-image: url('assets2/images/image_1.jpg');">
</a>
<div class="text p-4 float-right d-block">
<div class="d-flex align-items-center pt-2 mb-4">
<div class="one">
<span class="day">07</span>
</div>
<div class="two">
<span class="yr">2019</span>
<span class="mos">January</span>
</div>
</div>
<h3 class="heading mt-2"><a href="#">Why Lead Generation is Key for Business Growth</a></h3>
<p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
</div>
</div>
</div>
<div class="col-md-4 d-flex ftco-animate">
<div class="blog-entry justify-content-end">
<a href="blog-single.html" class="block-20" style="background-image: url('assets2/images/image_2.jpg');">
</a>
<div class="text p-4 float-right d-block">
<div class="d-flex align-items-center pt-2 mb-4">
<div class="one">
<span class="day">07</span>
</div>
<div class="two">
<span class="yr">2019</span>
<span class="mos">January</span>
</div>
</div>
<h3 class="heading mt-2"><a href="#">Why Lead Generation is Key for Business Growth</a></h3>
<p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
</div>
</div>
</div>
<div class="col-md-4 d-flex ftco-animate">
<div class="blog-entry">
<a href="blog-single.html" class="block-20" style="background-image: url('assets2/images/image_3.jpg');">
</a>
<div class="text p-4 float-right d-block">
<div class="d-flex align-items-center pt-2 mb-4">
<div class="one">
<span class="day">06</span>
</div>
<div class="two">
<span class="yr">2019</span>
<span class="mos">January</span>
</div>
</div>
<h3 class="heading mt-2"><a href="#">Why Lead Generation is Key for Business Growth</a></h3>
<p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
</div>
</div>
</div>
</div>
</div>
</section>

<section class="ftco-section-parallax">
<div class="parallax-img d-flex align-items-center">
<div class="container">
<div class="row d-flex justify-content-center">
<div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
<h2>Subcribe to our Newsletter</h2>
<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in</p>
<div class="row d-flex justify-content-center mt-4 mb-4">
<div class="col-md-8">
<form action="#" class="subscribe-form">
<div class="form-group d-flex">
<input type="text" class="form-control" placeholder="Enter email address">
<input type="submit" value="Subscribe" class="submit px-3">
</div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
</section> -->

<footer class="ftco-footer ftco-bg-dark ftco-section">
  <div class="container">
    <div class="row mb-5">
      <div class="col-md">
        <div class="ftco-footer-widget mb-4">
          <h2 class="ftco-heading-2">Gereja Kalvari Pentakosta</h2>
          <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
          <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
            <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
            <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
            <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
          </ul>
        </div>
      </div>
      <div class="col-md">
        <div class="ftco-footer-widget mb-4 ml-md-5">
          <h2 class="ftco-heading-2">Useful Links</h2>
          <ul class="list-unstyled">
            <li><a href="#" class="py-2 d-block">Speakers</a></li>
            <li><a href="#" class="py-2 d-block">Shcedule</a></li>
            <li><a href="#" class="py-2 d-block">Events</a></li>
            <li><a href="#" class="py-2 d-block">Blog</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md">
        <div class="ftco-footer-widget mb-4">
          <h2 class="ftco-heading-2">Privacy</h2>
          <ul class="list-unstyled">
            <li><a href="#" class="py-2 d-block">Career</a></li>
            <li><a href="#" class="py-2 d-block">About Us</a></li>
            <li><a href="#" class="py-2 d-block">Contact Us</a></li>
            <li><a href="#" class="py-2 d-block">Services</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md">
        <div class="ftco-footer-widget mb-4">
          <h2 class="ftco-heading-2">Address</h2>
          <div class="block-23 mb-3">
            <ul>
              <li><span class="icon icon-map-marker"></span><span class="text">Lorem ipsum dolor sit amet</span></li>
              <li><a href="#"><span class="icon icon-phone"></span><span class="text">0451 – 422611</span></a></li>
              <li><a href="#"><span class="icon icon-envelope"></span><span class="text">humas@pentakosta.co.id</span></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 text-center">

        <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
          Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart color-danger" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Anung Un Rama</a>
          <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
        </div>
      </div>
    </div>
  </footer>



  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="<?php echo base_url().'assets2/js/jquery.min.js'?>"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/3.1.0/jquery-migrate.min.js"></script>
  <script src="<?php echo base_url().'assets2/js/popper.min.js'?>"></script>
  <script src="<?php echo base_url().'assets2/js/bootstrap.min.js'?>"></script>
  <script src="<?php echo base_url().'assets2/js/jquery.easing.1.3.js'?>"></script>
  <script src="<?php echo base_url().'assets2/js/jquery.waypoints.min.js'?>"></script>
  <script src="<?php echo base_url().'assets2/js/jquery.stellar.min.js'?>"></script>
  <script src="<?php echo base_url().'assets2/js/owl.carousel.min.js'?>"></script>
  <script src="<?php echo base_url().'assets2/js/jquery.magnific-popup.min.js'?>"></script>
  <script src="<?php echo base_url().'assets2/js/aos.js'?>"></script>
  <script src="<?php echo base_url().'assets2/js/jquery.animateNumber.min.js'?>"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
  <script src="<?php echo base_url().'assets2/js/scrollax.min.js'?>"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="<?php echo base_url().'assets2/js/google-map.js'?>"></script>
  <script src="<?php echo base_url().'assets2/js/main.js'?>"></script>

</body>
</html>
