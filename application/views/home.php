
<!DOCTYPE html>
<html lang="en">

<head>
	
		<!-- Basic -->
    	<meta charset="UTF-8" />

		<title>Login</title>
	    <meta name="description" content="" />
		<meta name="author" content="" />
		<meta name="keyword" content="" />
		
		<!-- Mobile Metas -->
	    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		
		<!-- Import google fonts -->
        <link href="https://brimola.bri.co.id/brimola/assets/fonts/opensans.css" rel="stylesheet" type="text/css" />
        
		<!-- Favicon and touch icons -->
		<link rel="shortcut icon" href="" />
		<link rel="apple-touch-icon" href="https://brimola.bri.co.id/brimola/assets/admin/ico/touch-icon-iphone-60x60.html" />
		<link rel="apple-touch-icon" sizes="60x60" href="https://brimola.bri.co.id/brimola/assets/admin/ico/touch-icon-ipad-76x76.html" />
		<link rel="apple-touch-icon" sizes="114x114" href="https://brimola.bri.co.id/brimola/assets/admin/ico/touch-icon-iphone-retina-120x120.html" />
		<link rel="apple-touch-icon" sizes="144x144" href="https://brimola.bri.co.id/brimola/assets/admin/ico/touch-icon-ipad-retina-152x152.html" />
		
	    <!-- start: CSS file-->
		
		<!-- Vendor CSS-->
		<link href="https://brimola.bri.co.id/brimola/assets/admin/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
		<link href="https://brimola.bri.co.id/brimola/assets/admin/vendor/skycons/css/skycons.css" rel="stylesheet" />
		<link href="https://brimola.bri.co.id/brimola/assets/admin/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
		
		<!-- Plugins CSS-->		
		
		<!-- Theme CSS -->
		<link href="https://brimola.bri.co.id/brimola/assets/admin/css/jquery.mmenu.css" rel="stylesheet" />
		
		<!-- Page CSS -->		
		<link href="https://brimola.bri.co.id/brimola/assets/admin/css/style.css" rel="stylesheet" />
		<link href="https://brimola.bri.co.id/brimola/assets/admin/css/add-ons.min.css" rel="stylesheet" />
		
		<style>
			footer {
				display: none;
			}
		</style>
		
		<!-- end: CSS file-->	
	    
		
		<!-- Head Libs -->
		<script src="https://brimola.bri.co.id/brimola/assets/admin/plugins/modernizr/js/modernizr.js"></script>
		
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->		
		
	</head>

	<body>
		<!-- Start: Content -->
		<div class="container-fluid content">
			<div class="row">
				<!-- Main Page -->
				<div class="body-login">
					<div class="center-login">
						<a href="#" class="logo pull-left hidden-xs">
							<img src="https://brimola.bri.co.id/brimola/assets/admin/img/logo.png" height="45" alt="A-Plus Admin" />
						</a>

						<div class="panel panel-login">
							<div class="panel-title-login text-right">
								<h2 class="title"><i class="fa fa-user"></i> Login</h2>
							</div>
							<div class="panel-body">
								<div class="form-group">
																	
								</div>
								<form action="http://127.0.0.1/helloworld/index.php/home/login" role="form" id="login" name="login" method="post" accept-charset="utf-8">
									<div class="form-group">
										<label>Username</label>
										<div class="input-group input-group-icon">
											<input name="username" type="text" class="form-control bk-noradius" required />
											<span class="input-group-addon">
												<span class="icon">
													<i class="fa fa-user"></i>
												</span>
											</span>
										</div>
										<span id="msg_username" class="msg_username red"></span>
									</div>

									<div class="form-group">
										<label class="pull-left">Password</label>									
										<div class="input-group input-group-icon">
											<input name="password" type="password" class="form-control bk-noradius" required/>
											<span class="input-group-addon">
												<span class="icon">
													<i class="fa fa-lock"></i>
												</span>
											</span>
										</div>
										<span id="msg_password" class="msg_password red"></span>
									</div>
									<br />
									<div class="row">
										<div class="col-sm-12 text-right">
											<button type="submit" class="btn btn-primary hidden-xs">Login</button>
											<button type="submit" class="btn btn-primary btn-block btn-lg visible-xs bk-margin-top-10">Login</button>
										</div>
									</div>
									<br />
									<p class="text-center">Lupa Password? <a href="https://brimola.bri.co.id/brimola//users/forgot"><small>Reset Passwoord</small></a>
								</form>							</div>
						</div>
					</div>
				</div>
				<!-- End Main Page -->		
		
			</div>
		</div><!--/container-->
		
		
		<!-- start: JavaScript-->
		
		<!-- Vendor JS-->				
		<script src="https://brimola.bri.co.id/brimola/assets/admin/vendor/js/jquery.min.js"></script>
		<script src="https://brimola.bri.co.id/brimola/assets/admin/vendor/js/jquery-2.1.1.min.js"></script>
		<script src="https://brimola.bri.co.id/brimola/assets/admin/vendor/js/jquery-migrate-1.2.1.min.js"></script>
		<script src="https://brimola.bri.co.id/brimola/assets/admin/vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="https://brimola.bri.co.id/brimola/assets/admin/vendor/skycons/js/skycons.js"></script>	
		
		<!-- Plugins JS-->
		
		<!-- Theme JS -->		
		<script src="https://brimola.bri.co.id/brimola/assets/admin/js/jquery.mmenu.min.js"></script>
		<script src="https://brimola.bri.co.id/brimola/assets/admin/js/core.min.js"></script>
		
		<!-- Pages JS -->
		<script src="https://brimola.bri.co.id/brimola/assets/admin/js/pages/page-login.js"></script>
		
		<!-- end: JavaScript-->
		
	</body>
</html>